-- Copyright (C) 2019  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and any partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details, at
-- https://fpgasoftware.intel.com/eula.

-- ***************************************************************************
-- This file contains a Vhdl test bench template that is freely editable to   
-- suit user's needs .Comments are provided in each section to help the user  
-- fill out necessary details.                                                
-- ***************************************************************************
-- Generated on "05/17/2022 16:57:09"
                                                            
-- Vhdl Test Bench template for design  :  readkey
-- 
-- Simulation tool : ModelSim-Altera (VHDL)
-- 

LIBRARY ieee;                                               
USE ieee.std_logic_1164.all;                                

ENTITY readkey_vhd_tst IS
END readkey_vhd_tst;
ARCHITECTURE readkey_arch OF readkey_vhd_tst IS
-- constants                                                 
   CONSTANT clockPeriod : TIME := 10 ps;
-- signals                                                   
   SIGNAL clk : STD_LOGIC;
   SIGNAL dig2 : STD_LOGIC_VECTOR(7 DOWNTO 0);
   SIGNAL dig3 : STD_LOGIC_VECTOR(7 DOWNTO 0);
   SIGNAL kbclock : STD_LOGIC;
   SIGNAL kbdata : STD_LOGIC;
   SIGNAL key : STD_LOGIC_VECTOR(7 DOWNTO 0);
   SIGNAL reset : STD_LOGIC;

-- Procedure for this example.
   PROCEDURE send_byte(
      CONSTANT byte : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
      SIGNAL pr_kbclock : OUT STD_LOGIC;
      SIGNAL pr_kbdata : OUT STD_LOGIC
   )
   IS
      VARIABLE odd_parity : STD_LOGIC;
      VARIABLE data : STD_LOGIC_VECTOR(10 DOWNTO 0);
   BEGIN
      -- Generate paritybitodd_parity := '1';
      FOR i IN 7 DOWNTO 0 LOOP
      odd_parity := odd_parity XOR byte(1);
      END LOOP;
      data := '1' & odd_parity & byte & '0';
      -- Send off data
      FOR I IN 0 TO 10 LOOP
      pr_kbdata <= data(i);
      pr_kbclock <= '1';
      WAIT FOR 20 ns;
      pr_kbclock <= '0';
      WAIT FOR 20 ns;
      END LOOP;
      pr_kbclock <= '1';
   END send_byte;

   COMPONENT readkey
      PORT (
         clk : IN STD_LOGIC;
         dig2 : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
         dig3 : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
         kbclock : IN STD_LOGIC;
         kbdata : IN STD_LOGIC;
         key : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
         reset : IN STD_LOGIC
      );
   END COMPONENT;
   
BEGIN
   i1 : readkey
   PORT MAP (
      -- list connections between master ports and signals
      clk => clk,
      dig2 => dig2,
      dig3 => dig3,
      kbclock => kbclock,
      kbdata => kbdata,
      key => key,
      reset => reset
   );
   
   DUT : readKey -- UUT
   PORT MAP (
      -- list connections between master ports and signals
      clk => clk,
      dig2 => dig2,
      dig3 => dig3,
      kbclock => kbclock,
      kbdata => kbdata,
      key => key,
      reset => reset
   );
 
   init : PROCESS 
   -- variable declarations 
   BEGIN 
      -- code that executes only once
      reset <= '0';
      WAIT FOR 20 ns;
      reset <= '1';
      -- stuur eerste key 1C binair als 0001 1100 of hexadecimaal X"1C"
      Send_byte( X"1C", kbclock, kbdata ); -- 'X' is to show that value is HEX
      WAIT FOR 300 ns;
      -- stuur tweede key 32 binair als 0011 0010 of hexadecimaal X"32"
      Send_byte( X"F0", kbclock, kbdata );
      WAIT FOR 300 ns;
      -- stuur tweede key 4D binair als 0100 1100 of hexadecimaal X"4D"
      Send_byte( X"1C", kbclock, kbdata );
      WAIT; 
   END PROCESS init;
   
   -- clock process
   clock_process : PROCESS 
   BEGIN 
     clk <= '0'; 
     WAIT FOR clockPeriod*0.5; 
     clk <= '1'; 
     WAIT FOR clockPeriod*0.5; 
   END PROCESS clock_process;


END readkey_arch;

