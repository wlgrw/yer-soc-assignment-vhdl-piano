--------------------------------------------------------------------
--! \file      piano.vhd
--! \date      see top of 'Version History'
--! \brief     top-level entity for VHDL piano
--! \author    Remko Welling (WLGRW) remko.welling@han.nl
--! \copyright HAN TF ELT/ESE Arnhem 
--!
--! \todo Students that submit this code have to complete their details:
--!
--! Student 1 name         : 
--! Student 1 studentnumber: 
--! Student 1 email address: 
--!
--! Student 2 name         : 
--! Student 2 studentnumber: 
--! Student 2 email address: 
--!
--! Version History:
--! ----------------
--!
--! Nr:    |Date:      |Author: |Remarks:
--! -------|-----------|--------|-----------------------------------
--! 001    |23-8-2015  |WLGRW   |Inital version
--! 002    |13-2-2020  |WLGRW   |Update to MAX10 on DE10-Lite board
--! 003    |7-4-2020   |WLGRW   |Modification for assignment
--! 004    |6-7-2020   |WLGRW   |Added a todo not to modify the header of the file to represent the students that worked on the file.
--! 005    |20-3-2023  |WLGRW   |Added Documentation of DE0-CVL board layout.
--!
--! 
--! # Final assignment (7) YER-SOC
--!
--! Orgeltje is a composit of various VHDL components that is working as an organ using is PS2 keyboard.
--! This code is configured for usage with the Cyclone-V FPGA kit DE0-CV.
--! 
--! \verbatim
--!
--! Figure 1: Layout DE0-CV.                                              GPIO 0  GPIO 1
--!                                                                        +---+  +---+
--!      DE0-CV KEY, SW, LED, and HEX layout                               |. .|  |1 2| Pin 1 <- GPIO_1(0) Speaker out
--!                                                                        |. .|  |3 .|
--!                                                                        |. .|  |. .|
--!               7-segment displays (HEX)                                 |. .|  |. .|
--!              +---+---+---+---+---+---+                                 |. .|  |. .|
--!              |   |   |   |   |   |   |                                 |. .|  |.10|
--!              |   |   |   |   |   |   |                                 |. .|  |11.| Pin 11 <- GND for speaker
--!              |   |   | 8.| 8.| 8.| 8.|                                 |. .|  |. .|
--!              |   |   |   |   |   |   |                                 |. .|  |. .|
--!              |   |   |   |   |   |   |                                 |. .|  |. .|
--!              +---+---+---+---+---+---+                                ==============
--!                5   4   3   2   1   0                                   |. .|  |. .|
--!                                                                        |. .|  |. .|
--!      Number =>  9 8 7 6 5 4 3 2 1 0                                    |. .|  |. .|
--!                +-+-+-+-+-+-+-+-+-+-+                                   |. .|  |. .|
--! Leds (LEDR) => | | | | | | | | |A|B|                                   |. .|  |. .|
--!                +-+-+-+-+-+-+-+-+-+-+                                   |. .|  |. .|
--!                                                                        |. .|  |. .|
--!                +-+-+-+-+-+-+-+-+-+-+                                   |. .|  |. .|
--!                | | | | | | | | | | |   +--+--+--+--+  +--+             +---+  +---+
--!                +-+-+-+-+-+-+-+-+-+-+   |  |  |  |##|  |  |  <= KEY
--! Switch (SW) => | | | | | | | | | | |   +--+--+--+--+  +--+  
--!                +-+-+-+-+-+-+-+-+-+-+    3  2  1  0     4    <= Number
--!      Number =>  9 8 7 6 5 4 3 2 1 0              ^
--!                                                  |
--!                                                Reset
--! \endverbatim
--!
--! Functions:
--! - LEDR0 ON: Run, OFF: reset active
--! - LEDR1 ON: Audio is generated.
--! - KEY(0) pressed: RESET.
--!
--! Speaker is on GPIO_1 (JP2) pin 1: speaker output, GND pin 11. 
--!--------------------------------------------------------------------
LIBRARY ieee;
USE     ieee.STD_LOGIC_1164.all;
--------------------------------------------------------------------
ENTITY orgeltje IS
   PORT(
         CLOCK2_50     : IN  STD_LOGIC;                  --! 50 MHz clock on board the Cycone MAX 10 FPGA
         PS2_CLK,                                        --! PS2 keyboard clock signal (20 KHz domain) to PS2_KBCLK 
         PS2_DAT       : IN  STD_LOGIC;                  --! PS2 keybourd data signal (20 KHz domain) to PS2_KBDAT
         GPIO_1        : out STD_LOGIC_VECTOR(0 to 35);  --| Audio output signal on GPIO port
         HEX0, 
         HEX1,   
         HEX2, 
         HEX3,
         HEX4,
         HEX5          : OUT STD_LOGIC_VECTOR(0 to 6);   -- 7-segment displays HEX0 to HEX3
         KEY           : IN  STD_LOGIC_VECTOR(0 to 4);   -- Switches for reset (2)
         LEDR          : OUT STD_LOGIC_VECTOR(0 to 9)
        );
END orgeltje;
--------------------------------------------------------------------
ARCHITECTURE structure OF orgeltje IS
   
   --! add here SIGNALS 

BEGIN

   --! add here the structural VHDL that combines all COMPONENTS

END structure;