# YER SOC Assignment VHDL Piano by HAN

Project templates and materials for the VHDL Piano assignment tailored for use with the TerasIC DE0-CV FPGA prototype board. 

# Courses

This library is used in the following coures: 

 - YER-SOC

Please contact the lecturer of your course when the course you attend is not mentioned here. 

## Disclaimer
Project templates and materials for the VHDL ALU assignment in SOC class is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; Without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

## License
VHDL examples for use in SOC class are free: You can redistribute it and/or modify it under the terms of a Creative Commons Attribution-NonCommercial 4.0 International License (http://creativecommons.org/licenses/by-nc/4.0/) by Remko Welling (https://ese.han.nl/~rwelling/) E-mail: remko.welling@han.nl.

<a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">Creative Commons Attribution-NonCommercial 4.0 International License</a>.
